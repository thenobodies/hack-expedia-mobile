package com.expedia.hackathon.exp_hack.Util;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.expedia.hackathon.exp_hack.R;

public class View_Holder extends RecyclerView.ViewHolder {

    public CardView cv;
    public TextView title;
    public TextView description;
    public ImageView imageView;

    public View_Holder(View itemView) {
        super(itemView);
        cv = (CardView) itemView.findViewById(R.id.cardView);
        title = (TextView) itemView.findViewById(R.id.title);

    }
}
