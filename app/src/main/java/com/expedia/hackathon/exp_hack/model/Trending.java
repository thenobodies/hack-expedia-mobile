package com.expedia.hackathon.exp_hack.model;

import com.google.gson.annotations.SerializedName;

import java.net.PortUnreachableException;

public class Trending {
    @SerializedName("location")
    public String location;
    @SerializedName("id")
    public int id;


    public Trending(String location,int id) {
        this.location = location;
        this.id=id;

    }

}
